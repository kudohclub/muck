const scrapeIt = require("scrape-it");
const excelbuilder = require('msexcel-builder');

const scrape_url = 'https://www.boostmyscore.net/boostmycreditscore/view-our-tradelines/';
const output_file = 'tradelines.xlsx';

scrapeIt(scrape_url, {
    title: "h1",
    tradelines: {
        listItem: '.tradeline-row',
        data: {
            id: {
                attr: 'id',
                convert: x => x.replace('tradeline-', '')
            },
            age_years: { attr: 'data-age'  },
            age_days: { attr: 'data-days' },
            type: {
                selector: '.tradeline-type',
                convert: x => x.replace(/[0-9]/g, '')
            },
            details: {
                selector: '.tradeline-limit',
                how: 'html', 
                convert: x => {
                    const parts = x.split('<br>')
                    const limit = parts[0].replace('$', '').split('-'); 
                    const age = parts[1].replace(' months', '').split('-');
                    return {
                        limit: {
                            min: limit[0],
                            max: limit[1]
                        },
                        age: {
                            min: age[0],
                            max: age[1]
                        }
                    }
                }
            },
            cost: {
                selector: '.tradeline-cost',
                how: 'html',
                convert: x => x.split('$')[2]
            }
        }
    }
}).then(page => {    
    var workbook = excelbuilder.createWorkbook('./', output_file);
    var sheet = workbook.createSheet('Boost My Score', 300, 300);

    sheet.set(1, 1, "ID");
    sheet.set(2, 1, "TYPE");
    sheet.set(3, 1, "AGE (YR)");
    sheet.set(4, 1, "AGE (DAYS)");
    sheet.set(5, 1, "LIMIT (MIN)");
    sheet.set(6, 1, "LIMIT (MAX)");
    sheet.set(7, 1, "COST");

    page.tradelines.forEach((tradeline, index) => {
        sheet.set(1, index+2, tradeline.id);
        sheet.set(2, index+2, tradeline.type);
        sheet.set(3, index+2, tradeline.age_years);
        sheet.set(4, index+2, tradeline.age_days);
        sheet.set(5, index+2, tradeline.details.limit.min);
        sheet.set(6, index+2, tradeline.details.limit.max);
        sheet.set(7, index+2, tradeline.cost);
    });

    workbook.save((ok) => {
        if (!ok)
            console.log('Error');
        else {
            console.log(`Done! Exported to ./${output_file}`);
        }
    });

}).catch(error => {
    console.log(error);
});